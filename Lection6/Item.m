//
//  Item.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "Item.h"


@implementation Item

+ (Item *)createItemWithTitle:(NSString *)title image:(NSString *)image {
    Item *item = [[Item alloc] init];
    item.title = title;
    item.imageName = image;
    return item;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _UUID = [[NSUUID UUID] UUIDString];
    }
    return self;
}

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        _UUID = [aDecoder decodeObjectForKey:@"UUID"];
        _title = [aDecoder decodeObjectForKey:@"title"];
        _detail = [aDecoder decodeObjectForKey:@"detail"];
        _imageName = [aDecoder decodeObjectForKey:@"imageName"];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    Item *item = [[Item alloc] init];
    item.UUID = self.UUID;
    item.title = self.title;
    item.detail = self.detail;
    item.imageName = self.imageName;
    return item;
}

- (NSUInteger)hash {
    return [self.UUID hash];
}

- (BOOL)isEqual:(id)object {
    return [[object UUID] isEqual:self.UUID];
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.UUID forKey:@"UUID"];
    [aCoder encodeObject:self.detail forKey:@"detail"];
    [aCoder encodeObject:self.imageName forKey:@"imageName"];
}


@end
