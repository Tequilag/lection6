//
//  Item.h
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject <NSCopying, NSCoding>

@property (nonatomic, strong) NSString *UUID;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *detail;

@property (nonatomic, strong) NSString *imageName;

+ (Item *)createItemWithTitle:(NSString *)title image:(NSString *)image;

@end
