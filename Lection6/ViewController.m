//
//  ViewController.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "ViewController.h"
#import "CustomView.h"
#import "Model.h"
#import "Item.h"
#import "EditViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate, EditViewControllerDelegate>

@property (nonatomic, strong) CustomView *customView;
@property (nonatomic, strong) Model *model;

@end

@implementation ViewController

- (void)loadView {
    self.customView = [[CustomView alloc] init];
    [self setView:self.customView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[Model alloc] init];
    
    self.customView.table.dataSource = self;
    self.customView.table.delegate = self;
    
    self.customView.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(createItem:)];
}

- (void)createItem:(id)sender {
    EditViewController *editController = [[EditViewController alloc] init];
    editController.delegate = self;
    
    [self.navigationController pushViewController:editController animated:YES];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MYCell"];
    }
    
    Item *item = [self.model.items objectAtIndex:indexPath.row];
    cell.textLabel.text = item.title;
    cell.imageView.image = [UIImage imageNamed:item.imageName];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Item *item = [self.model.items objectAtIndex:indexPath.row];
    
    EditViewController *editController = [[EditViewController alloc] init];
    editController.item = item;
    editController.delegate = self;
    
    [self.navigationController pushViewController:editController animated:YES];
}

#pragma mark - EditViewControllerDelegate

- (void)editViewController:(EditViewController *)controller didCreateItem:(Item *)item {
    
    NSUInteger index = [self.model.items indexOfObject:item];
    
    if (index == NSNotFound) {
        [self.model.items addObject:item];

    }
    else {
        [self.model.items replaceObjectAtIndex:index withObject:item];
    }
    
    [self.customView.table reloadData];
    [self.model save];
}

@end
